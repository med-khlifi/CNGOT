<?php

namespace CNGOTBundle\Controller;

use CNGOTBundle\Entity\Actualite;
use CNGOTBundle\Form\ActualiteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use CNGOTBundle\Entity\Video;
use CNGOTBundle\Form\VideoType;

class ActualiteController extends Controller
{
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $videos = $em->getRepository('CNGOTBundle:Video')->findAll();
        
        return $this->render('CNGOTBundle:video:index.html.twig', array('videos' => $videos));
    }
    public function addActualiteAction(Request $request)
    {
   $document = new Actualite();
   $form = $this->createForm(new ActualiteType(), $document);
    $form->handleRequest($request);

    if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $document->upload();
        $em->persist($document);
        $em->flush();

        return $this->redirectToRoute('add_actualite', array());
    }

        $em = $this->getDoctrine()->getManager();
        $Actualite= $em->getRepository('CNGOTBundle:Actualite')->findAll();

    return $this->render('CNGOTBundle:admin:addActualite.html.twig',array('form' =>$form->createView(),'Actualite' => $Actualite));
    }
    
    public function viewAction($id) {
        $em = $this->getDoctrine()->getManager();
        $video = $em->getRepository('CNGOTBundle:Video')->find($id);
        
        return $this->render('CNGOTBundle:video:view.html.twig', array('video' => $video));
    }
    
     public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $video = $em->getRepository('CNGOTBundle:Actualite')->find($id);


          $em->remove($video);
        $em->flush();
        
        return $this->redirectToRoute("add_actualite");
    }

    public function modifActualiteAction($id) {


        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('CNGOTBundle:Actualite')->find($id);
        $form = $this->createform(new ActualiteType(), $user);
        $request = $this->getRequest();
        if ($request->getMethod() == "POST") {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->get('doctrine')->getEntityManager();
                $em->persist($user);
                $em->flush();

                return $this->redirect($this->generateUrl('add_actualite'));
            }
        }

        return $this->render('CNGOTBundle:admin:modifAct.html.twig', array('form' => $form->createView(), 'id' => $id));
    }

}
