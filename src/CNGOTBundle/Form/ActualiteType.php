<?php

namespace CNGOTBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActualiteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('titre')->add('descreption')->add('dateAjout', 'date', array(
            'format' => \IntlDateFormatter::SHORT,
            'attr'=>array('style'=>'display:none;'),
            'data' => new \DateTime("now")))->add('file', 'file',array(
        'required' => false))        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CNGOTBundle\Entity\Actualite'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'cngotbundle_actualite';
    }


}
